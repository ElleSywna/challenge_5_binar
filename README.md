// Nama: Helen
// FSW-1

DB Diagram :
https://dbdiagram.io/d/626412e51072ae0b6ad6b0e0



# Info
- Get all cars : GET <code>/api/cars</code>
- Create a car : POST <code>/api/cars/create</code>
- Update a car : PUT <code>/api/cars/update/:id</code>
- Delete a car : DELETE <code>/api/cars/delete/:id</code>

- Filter size car : GET <code>/api/cars/search/?car_size=</code>
- Search cars by name and size : GET <code>/api/cars/search/?q=</code>